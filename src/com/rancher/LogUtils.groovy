package com.rancher
class LogUtils {

    static final String COLOR_GREEN = '\033[32m'
    static final String COLOR_RED = '\033[31m'
    static final String COLOR_END = '\033[0m'

}