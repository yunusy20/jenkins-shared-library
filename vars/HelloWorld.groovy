import com.rancher.Constants
def call(String name = "Alice") {
    script {
        sh """
            echo Hi ${name}
            echo ${Constants.SLACK_MESSAGE}
        """
    }
}